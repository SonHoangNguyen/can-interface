CC = gcc
CFLAGS = -std=gnu99 -fPIC -Wall -Werror -Wextra -Wno-unused -O2 -I .
LDFLAGS = -std=gnu99 -Werror -L .
LDFLAGS_SO = $(LDFLAGS) -shared -fPIC


.SUFFIXES: .c .h .o .so
.PHONY: all  clean


CanSend_SRC = CanSend.c lib.c
CanSend_OBJ = $(CanSend_SRC:.c=.o)
CanSend_TARG = CanSendData


## Targets ##

all: $(CanSend_TARG)

$(CanSend_TARG): $(CanSend_OBJ) 
	$(CC) $(CanSend_OBJ) -o $(CanSend_TARG)

clean:
	$(RM) -r $(SOCKETCAN_OBJ) $(CANOPEN_OBJ) $(MOTOR_OBJ) $(CanSend_OBJ)
	$(RM) -r $(SOCKETCAN_LIB) $(CANOPEN_LIB) $(MOTOR_LIB) $(CanSend_TARG)

%.o: %.c $(wildcard *.h)
	$(CC) -c $< $(CFLAGS) -o $@